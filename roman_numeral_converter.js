function convertToRoman(num) {
    if(num <= 0 || num > 3999) return false;
    const romans = {1:'I',4:'IV',5:'V',9:'IX',10:'X',40:'XL',50:'L',90:'XC',100:'C',400:'CD',500:'D',900:'CM',1000:'M'};
    const findRomanLetter = function (num,scale) {
            const n = num*scale;
            if(romans[n]) {
                return romans[n];
            } else if (n > 1 * scale && n < 4 * scale) {
                return romans[1 * scale].repeat(n/scale);
            } else if (n > 5 * scale && n < 9 * scale) {
                return romans[5 * scale] + romans[1 * scale].repeat((n/scale) - 5);
            }
        };
    const romanNum = String(num).split('').map((x,index,array) => {
        if(x === '0') return '';
        const dec = [1,10,100,1000];
        for (let i = 0; i<=3; i++){
            if(index === array.length - (i+1)) {
                return findRomanLetter(x, dec[i]);
            };
        }
    }).join('');

    return romanNum;
}